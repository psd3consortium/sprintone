package login;

import java.util.Scanner;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import admin.admin;
import lecturer.lec;
import student.stuSign;

public class Activator implements BundleActivator {
	
	 ServiceReference lectReference;
	 ServiceReference studentReference;
	 ServiceReference adminReference;

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
	
		System.out.println("Please Login");
		
		Scanner UP = new Scanner(System.in);
		System.out.println("Enter user role.");
		String UR = UP.nextLine();
		
		//do login here
		
		if ( UR.equals( "admin" ) ) {
			//ADMIN STUFF
			
			adminReference = context.getServiceReference(admin.class.getName());
			admin AdminService = (admin)context.getService(adminReference);
			
			//Start of admin case
			
			String choice;
			Scanner in = new Scanner(System.in);
			System.out.println("\nWelcome Admin, please select options.\nPress 1 to create Timetable.\nPress 2 to add venue.\nPress 3 to check for clashes.");
			choice = in.nextLine();
			int choose =Integer.parseInt(choice);
			
			switch (choose){
			case 1:
				System.out.println(AdminService.createNewTimetable());
				break;
			case 2:
				System.out.println(AdminService.addRoom());
				break;
			case 3:
				System.out.println(AdminService.checkClash());
				break;
			default:
				System.out.println("Wrong Input");
			   	break;
			}
		    
			//End of admin case
			
		} else if (UR.equals("lecturer")) {
			//LECTURER STUFF
			
			lectReference = context.getServiceReference(lec.class.getName());
			lec LecturerService = (lec)context.getService(lectReference);
			
			//Start of lecturer case
			
			String choice;
			Scanner in = new Scanner(System.in);
			System.out.println("\nWhat do you want to do?\nPress 1 to view session.\nPress 2 to import courses.");
			choice = in.nextLine();
			int choose =Integer.parseInt(choice);
			
			switch (choose){
			case 1:
				System.out.println(LecturerService.getSessionLect());
				break;
			case 2:
				System.out.println(LecturerService.addSessionLect());
				break;
			default:
				System.out.println("Wrong Input");
			   	break;
			}
		    
			//End of lecturer case
			
		} else if (UR.equals("student")) {
			//STUDENT STUFF
			
			studentReference = context.getServiceReference(stuSign.class.getName());
			stuSign StudentService = (stuSign)context.getService(studentReference);
			
			//Start of student case
			
			System.out.println("Welcome to Student Mangement System");
			
			String choice;
			Scanner in = new Scanner(System.in);
			System.out.println("Choose your option");
			System.out.println("1. Sign up for course");
			System.out.println("2. Check if signed up for compulsory sessions");
			choice = in.nextLine();
			int selection =Integer.parseInt(choice);
			
			switch (selection){
			case 1:
				
				
				Scanner scanName = new Scanner(System.in);
				System.out.println("Enter name.");
				String user = scanName.nextLine();
				
				System.out.println(StudentService.signUp(user));
				
				break;
			case 2:
				
				scanName = new Scanner(System.in);
				System.out.println("Enter name.");
				user = scanName.nextLine();
				
				System.out.println(StudentService.checkCompulsory(user));
				
				break;
		   	default:
		   	   System.out.println("Wrong input");
		   	   break;
		      }
		
			
			
		} else if (UR.equals("tutor")){
			//TUTOR STUFF
		
		
		} else {
			
			System.out.println("No such role exist");
		}
		
		
	}
	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		
		context.ungetService(lectReference);
		context.ungetService(studentReference);
		context.ungetService(adminReference);
		System.out.println("Exit Success");
	}

}
