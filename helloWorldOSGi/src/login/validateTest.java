package login;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Scanner;

import org.hamcrest.core.IsEqual;
import org.junit.Test;

public class validateTest {

	
	@Test
	public void testValidation() {
		
		String userInput = "assassin";
		
		if (userInput.equals("admin")) {
			
			assertEquals(userInput, "admin");
			
		} else if (userInput.equals("lecturer")){
			
			assertEquals(userInput, "lecturer");
			
		} else if (userInput.equals("student")) {
			
			assertEquals(userInput, "student");
			
		} else  if (userInput.equals("tutor")) {
			
			assertEquals(userInput, "tutor");
			
		} else {
			assertNotEquals(userInput, "admin");
			assertNotEquals(userInput, "student");
			assertNotEquals(userInput, "lecturer");
			assertNotEquals(userInput, "tutor");
			
			
		}
	}
}
