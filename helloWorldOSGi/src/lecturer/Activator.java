package lecturer;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

import lecturer.lecImpl;

public class Activator implements BundleActivator {
	ServiceRegistration lecturerRegistration;
	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		
		 lec lect = new lecImpl();
		 lecturerRegistration = context.registerService(lec.class.getName(), lect, null);
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		lecturerRegistration.unregister();
	}

}
