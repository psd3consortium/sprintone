package lecturer;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class ImportCourses {
	
	public ImportCourses() {

	}

	public ArrayList<String> createUploader(String filename)
			throws FileNotFoundException {
		Scanner scanner = new Scanner(new File(filename));
		ArrayList<String> strArray = new ArrayList<String>();

		scanner.useDelimiter(",");

		int i = 1;
		strArray.add(scanner.next());
		while (scanner.hasNext()) {
			if (i % 2 == 0) {
				String tmpString = scanner.next().substring(2);
				strArray.add(tmpString);
			}
			scanner.next();
			i++;
		}

		scanner.close();
		return strArray;
	}
}
