package lecturer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class lecImpl implements lec {


	@Override
	public String getSessionLect() {
			    
          File f = new File("/Users/user/Workspace/helloWorldOSGi/src/session.csv");
           
    	  if(!f.exists()){
    		 
    		  System.out.println("File not found!");
    	  
    	  }else{    	  
          
		String csvFile = "/Users/user/Workspace/helloWorldOSGi/src/session.csv";
        		
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
	
		ArrayList<String> allSession = new ArrayList<String>();
		
		try {
		  		
				br = new BufferedReader(new FileReader(csvFile));
				while ((line = br.readLine()) != null) {
					allSession.add(line);
				}
		 
			} 
			catch (FileNotFoundException e) {
				e.printStackTrace();
			} 
			catch (IOException e) {
				e.printStackTrace();
			} 
			finally {
				if (br != null) {
					try {
						br.close();
					} 
					catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		
			String header = allSession.get(0);
			String[] headerInfo = header.split(cvsSplitBy);
				for(int i = 1; i < allSession.size(); i++){
						line = allSession.get(i);

						String[] details = line.split(cvsSplitBy);
			
				
						System.out.println(" Details:"
	                               + "\n  Course Name: " + details[0]
			                       + "\n  Lecturer Name: " + details[1]
			                       + "\n  Start Time: " + details[2]
			                       + "\n  End Time: " + details[3]
			                       + "\n  Venue: " + details[4]
			                       + "\n  Tutor Name: " + details[5]   
			                       + "\n  Compulsory: " + details[6]);
			                    for( int u = 7; u < details.length; u++){
			       					System.out.println("  Students: " + details[u]);		
			       						}
			            System.out.println("");
			 }
		}
			
	    return "End of session.csv";
	}

	@Override
	public String addSessionLect() throws IOException {
	
		ArrayList<String> arrayCourses = new ArrayList<String>();
		ImportCourses IC1 = new ImportCourses();
		arrayCourses = IC1.createUploader("/Users/user/Workspace/helloWorldOSGi/src/Courses.csv");
		System.out.print("Array:" + arrayCourses);

		// Select course in which to add session
		System.out.println("Please select the course:");

		for (int i = 0; i < arrayCourses.size(); i++) {
			System.out
					.println("Course " + (i + 1) + ": " + arrayCourses.get(i));
		}

		Scanner scanner = new Scanner(System.in);
		String choice = scanner.next();

		int intChoice = Integer.parseInt(choice) - 1;
		System.out.println(arrayCourses.get(intChoice) + " has been selected.");

		System.out.println("Please input your Lecturer Name:");
		Scanner scanner1 = new Scanner(System.in);
		String lecturername = scanner1.next();
		System.out.println("The lecturer is " + lecturername);
		
		System.out.println("Please input your desired starttime:");
		Scanner scanner2 = new Scanner(System.in);
		String starttime = scanner2.next();
		System.out.println("The starttime is " + starttime);
		
		System.out.println("Please input your desired endtime:");
		Scanner scanner3 = new Scanner(System.in);
		String endtime = scanner3.next();
		System.out.println("The endtime is " + endtime);

		System.out.println("Please input your desired venue:");
		Scanner scanner4 = new Scanner(System.in);
		String venue = scanner4.next();
		System.out.println("The venue is at " + venue);

		System.out.println("Please input your desired tutor: (NULL for no tutor)");
		Scanner scanner5 = new Scanner(System.in);
		String tutor = scanner5.next();
		System.out.println("The tutor is " + tutor);

		System.out.println("Please indicate whether this session is compulsory: Y for yes, N for no");
		Scanner scanner6 = new Scanner(System.in);
		String isCompulsory = scanner6.next();
		String strCompulsory;
		if (isCompulsory.equals("Y")) {
			strCompulsory = "is compulsory";
		} else {
			strCompulsory = "is not compulsory";
		}
		System.out.println("The session " + strCompulsory);

		System.out.println("The entered session is "
				+ arrayCourses.get(intChoice) + " " + lecturername + " "+ starttime + " " + endtime + " " + " " + venue
				+ " " + tutor + " " + strCompulsory + ".");

		// Append added session into csv.
		String filename = "/Users/user/Workspace/helloWorldOSGi/src/session.csv";
		FileWriter fw = new FileWriter(filename, true); // the true will append
		// the new data

		fw.write(arrayCourses.get(intChoice) + "," + lecturername + "," + starttime + "," + endtime + "," + venue
				+ "," + tutor + "," + isCompulsory + "," + "\n");
		fw.close();
		
		return "Added Details!";
	}
}
