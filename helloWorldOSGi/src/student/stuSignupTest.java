package student;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class stuSignupTest {

	stuSignup student;
	String user;
	stuSignup testStud = new stuSignup();
	@BeforeClass

	
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		user = "WinWin";
	}

	@After
	public void tearDown() throws Exception {
		user = null;
	}

	@Test
	public void testSignUp() {
		testStud.signUp(user);
		assertEquals(14,(testStud.totalCol.length));
	}

	@Test
	public void testCheckCompulsory() {
		//fail("Not yet implemented");
		//String user = "dsadsadas";
		student = new stuSignup();
		String output = student.checkCompulsory(user);
		if(user == "WinWin"){
			assertEquals("All compulsory course is registered", output);
		}
		else if(user == "Edwin"){
			assertEquals("null1. Course Name: PSD, Lecturer Name: AF, Start Time: 1400, End Time: 1600, Venue: Lab, Tutor Name: 2. Course Name: PSD3, Lecturer Name: Boatman, Start Time: 1400, End Time: 1600, Venue: ferry, Tutor Name: NULL", output);
		}
	}
	/*@Test
	public void testNotCheckCompulsory() {
		student = new stuSignup();
		String output = student.checkCompulsory(user);
		assertNotSame("All compulsory course is registered", output);
	}
*/
}
