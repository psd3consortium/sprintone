package student;

import java.util.ArrayList;
import java.util.Scanner;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import lecturer.lec;
import lecturer.lecImpl;

public class Activator implements BundleActivator {
	ServiceRegistration studentServiceRegistration;
	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		
		 stuSign studentSignUp = new stuSignup(); 
		 studentServiceRegistration = context.registerService(stuSign.class.getName(), studentSignUp, null);

	}


	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		//System.out.println("Goodbye World!!");
		studentServiceRegistration.unregister();
	}

}
