package admin;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

import lecturer.lecImpl;

public class Activator implements BundleActivator {
	ServiceRegistration adminServiceRegistration;
	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		
		 admin administrator = new adminImpl();
		 adminServiceRegistration = context.registerService(admin.class.getName(), administrator, null);
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		adminServiceRegistration.unregister();
	}

}
