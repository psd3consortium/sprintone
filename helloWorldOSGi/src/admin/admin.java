package admin;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface admin {
	public String addRoom() throws Exception;
	
	public String createNewTimetable() throws IOException;
	
	public String checkClash() throws Exception;

}
