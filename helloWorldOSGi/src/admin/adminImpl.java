package admin;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class adminImpl implements admin {

	ArrayList<String> getSession = new ArrayList<String>();
	@Override
	public String addRoom() throws Exception {
			  
		  String results = "";
		  String csvFile =  "/Users/user/Workspace/helloWorldOSGi/src/session.csv";
		  BufferedReader br = null;
		  String line = "";
		  
		br = new BufferedReader(new FileReader(csvFile));
		
		while ((line = br.readLine()) != null) {			
					getSession.add(line);
				}
		br.close();
		
			String cvsSplitBy = ",";
			
			for(int i=1; i<getSession.size(); i++){
				line = getSession.get(i);
				
				String[] info = line.split(cvsSplitBy);
				
				System.out.println(i + ". Course Name: " + info[0] + "\tLecturer Name: " + info[1] + "\tStart Time: " + info[2] + "\tEnd Time: " + info [3] + "\tVenue: " + info[4] + "\tTutor Name: " + info[5] + "\tCompulsory: " + info[6]);
			}
			System.out.println("");
			System.out.println("Choose which course to update its venue.");
			
			Scanner scan = new Scanner(System.in);
			
			String getInput = scan.next();
			int getUserInput = Integer.parseInt(getInput);
			
			System.out.println("Key in the new venue");
			
			Scanner scanVenue = new Scanner(System.in);
			String stringVenue = scanVenue.next();
			
			String newSession = "";
			
			String line2 = "";
			String[] info2;
			
			for(int x = 1; x<getSession.size(); x++){
			
			if(getUserInput == x){
			
					line2 = getSession.get(x);
					info2 = line2.split(cvsSplitBy);
				//	System.out.println("Course Name: " + info2[0] + "\tLecturer Name: " + info2[1] + "\tStart Time: " + info2[2] + "\tEnd Time: " + info2[3] + "\tVenue: " + info2[4] + "\tTutor Name: " + info2[5] + "\tCompulsory: " + info2[6]);
					
					info2[4] = stringVenue;
					
				for(int a=0; a<info2.length; a++){
					newSession += info2[a] + ",";
				}
				
				System.out.println(newSession);
				getSession.add(newSession);
				getSession.remove(x);
				break;
				
			}
			
			}
			
				File newFile = new File("/Users/user/Workspace/helloWorldOSGi/src/session.csv");
				if(newFile.exists()){
				    newFile.delete();
				    
				    FileWriter writer = new FileWriter(newFile, true);
				    
				    String line3 = "";
				    
					for(int i=0; i<getSession.size(); i++){
						
						line3 = getSession.get(i);
						writer.append(line3+'\n');
					}
					writer.close();
					results = ("Venue updated sucessfully!");
				}
 
	    return results;
	}

	@Override
	public String createNewTimetable() throws IOException {
		
		 String reply = "";
		 File f = new File("/Users/user/Workspace/helloWorldOSGi/src/session.csv");
         
   	  if(!f.exists()){
   		 
   		FileWriter writer = new FileWriter("/Users/user/Workspace/helloWorldOSGi/src/session.csv");
   	 
   		//create the header 
   	
	    writer.append("course_name");
	    writer.append(',');
	    writer.append("lecturer_name");
	    writer.append(',');
	    writer.append("start_time");
	    writer.append(',');
	    writer.append("end_time");
	    writer.append(',');
	    writer.append("venue");
	    writer.append(',');
	    writer.append("tutor_name");
	    writer.append(',');
	    writer.append("compulsory");
	    writer.append('\n');
	    
	    writer.flush();
	    writer.close();
	    
   		  reply = "Timetable is created!";
   	  
   	  } else {    	  
   		 reply = "Timetable exist!";
   	  }
		return reply;
	}
	
	public String checkClash() throws IOException {

		  String results = "";
		  String csvFile =  "/Users/user/Workspace/helloWorldOSGi/src/session.csv";
		  BufferedReader br = null;
		  String line = "";
		  String line2 = "";
		  String result = "Clash Found!\n";
		br = new BufferedReader(new FileReader(csvFile));
		
		while ((line = br.readLine()) != null) {			
					getSession.add(line);
				}
		br.close();
		
			String cvsSplitBy = ",";
			
			for(int i=1; i<getSession.size(); i++){
				line = getSession.get(i);
				
				String[] info = line.split(cvsSplitBy);
				
			for (int j=1; j<getSession.size(); j++){
				line2 = getSession.get(j);
				String[] info2 = line2.split(cvsSplitBy);
				
				if (i != j) {
					
					if (info[0].equals(info2[0]) ){ //course name
						
						if (info[4].equals(info2[4])) { //check venue
						
						if (info[2].equals(info2[2])) { //start time
							
							if (info[3].equals(info2[3])){ //end time
								
							result +=("Row number: " + i+" Course Name: " + info[0] + "\tLecturer Name: " + info[1] + "\tStart Time: " + info[2] + "\tEnd Time: " + info [3] + "\tVenue: " + info[4] + "\tTutor Name: " + info[5] + "\tCompulsory: " + info[6] + "\n");	
							}	else {
								result = ("No Clash Found!");
							}
						}	
					 }
				   }
				 }	
		       }		
			}
		
			System.out.println(result);
		
		 return result;
		
	}

	
}
