package admin;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import org.hamcrest.core.IsEqual;
import org.junit.Test;

public class createTimeTableTest {

	adminImpl admin = new adminImpl();
	@Test
	public void testCreateNewTimetable() throws IOException {
		
	 String reply = admin.createNewTimetable();
	 
	 String existed = "Timetable exist!";
	 
	 String created = "Timetable is created!";
	 
	 if (reply.equals(created)){
	 
		 System.out.println(reply);
		 assertEquals(created, reply);		
	
	 } else {
		
		 System.out.println(reply);
		 assertEquals(existed, reply);	
	 }
	
	}
}
