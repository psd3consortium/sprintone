# Dependencies

You will need to have `ant` dependencies installed. 

# Build Instructions

1) Open a Terminal Program.

2) Run the following commands:

	ant bundles

	ant run

	lb

	install file:lecturer.jar

3) To install the jar files:

	install file:admin.jar

	install file:student.jar

	install file:login.jar

4) To start the program

	start id

5) Upon program end, 

	uninstall id

	stop 0